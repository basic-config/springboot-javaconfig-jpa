package net.suby.project.user.controller;

import net.suby.project.user.dao.mysql1.UserRepository1;
import net.suby.project.user.dao.mysql2.UserRepository2;
import net.suby.project.user.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

/**
 * Created by myborn on 2016-12-18.
 */
@Controller
@RequestMapping("/member")
public class UserController {
    @Autowired
    private UserRepository1 memberDao1;

    @Autowired
    private UserRepository2 memberDao2;

    @RequestMapping("/list")
    public String list(Model model) {
        List<UserVO> list = memberDao2.findAll();
        System.out.println(memberDao2);
        for(UserVO uservo : list){
            System.out.println(uservo.getUserNm());
        }
        return "list";
    }

    @RequestMapping("/get/{id}")
    public void del(@PathVariable String id) {
        UserVO uservo = memberDao1.findOne(id);
        System.out.println(memberDao1);
        System.out.println(uservo.getUserNm());
    }
}
