package net.suby.project.user.dao.mysql1;

import net.suby.project.user.vo.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by myborn on 2016-12-18.
 */
@Repository
public interface UserRepository1 extends JpaRepository<UserVO, String> {
}
