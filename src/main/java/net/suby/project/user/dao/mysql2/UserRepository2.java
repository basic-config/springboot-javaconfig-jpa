package net.suby.project.user.dao.mysql2;

import net.suby.project.user.vo.UserVO;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by myborn on 2016-12-18.
 */
public interface UserRepository2 extends JpaRepository<UserVO, String> {
}
