package net.suby.project.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.HashMap;

/**
 * Created by myborn on 2016-12-25.
 */
@Configuration
@PropertySource({ "classpath:application.properties" })
@EnableJpaRepositories(
        basePackages = "net.suby.project.user.dao.mysql2",
        entityManagerFactoryRef = "mysql2EntityManager",
        transactionManagerRef = "mysql2TransactionManager"
)
public class PersistenceJpaConfig2 {
    @Autowired
    private Environment env;

    @Bean
    public LocalContainerEntityManagerFactoryBean mysql2EntityManager() {
        LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(mysql2DataSource());
        em.setPackagesToScan(new String[] { "net.suby.project.user.vo" });

        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        em.setJpaVendorAdapter(vendorAdapter);
        HashMap<String, Object> properties = new HashMap<String, Object>();
        properties.put("hibernate.hbm2ddl.auto", env.getProperty("hibernate.hbm2ddl.auto"));
        properties.put("hibernate.dialect", env.getProperty("hibernate.dialect"));
        em.setJpaPropertyMap(properties);

        return em;
    }

    @Bean
    public DataSource mysql2DataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring2.datasource.driverClassName"));
        dataSource.setUrl(env.getProperty("spring2.datasource.url"));
        dataSource.setUsername(env.getProperty("spring2.datasource.username"));
        dataSource.setPassword(env.getProperty("spring2.datasource.password"));

        return dataSource;
    }

    @Bean
    public PlatformTransactionManager mysql2TransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(mysql2EntityManager().getObject());
        return transactionManager;
    }
}
